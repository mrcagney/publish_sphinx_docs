Publish Sphinx Docs
********************
A simple Python 3.6+ CLI to publish your Sphinx documentation to a
Github Pages or Gitlab Pages repository.


Installation
=============
``poetry add publish_sphinx_docs``


Usage
=====
At the command line, enter ``publish-sphinx-docs --help``.


Authors
========
- Alex Raichev (2018-12-03), maintainer


Documentation
=============
On Github Pages `here <https://mrcagney.github.io/publish_sphinx_docs_docs/>`_ and built using this library :-)


Notes
=====
- Experimental, so use at your own risk.


Changes
=======

1.0.2, 2021-10-07
-----------------
- Updated dependencies again.


1.0.2, 2021-10-07
-----------------
- Updated dependencies.


1.0.1, 2019-04-04
-----------------
- Removed sphinx-click from dependencies, because it was meant to be a development dependency.


1.0.0, 2018-12-04
------------------
- First release.
